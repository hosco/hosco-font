## For designers

### Publish a new version
Add the generated files in this repository:

- The svg files and the `selection.json` file (used by web-lego)
- The scss files + the fonts folder (used by web-hoscov2)
- The demo-files so we can easily browse the icons in the browser

Update the version in the `package.json` file

### Steps to follow in the Terminal

- If you wanna know where you are in the folder structure, enter command `pwd`
- Enter command `cd ~/Documents/hosco-font/` (the path of this repo on your local machine)
- Enter command `git pull`
- Add your files in the repo folder
- Enter command `git status -s` (`M` Stands for modified files `??`stands for new files)
- Enter command `git commit -am "comment or description of the update or commit name"`
- Enter command `git push`


### Naming
- The icons should be named in kebab case, ex: `application-sent` and not `applicationSent` or `applicationsent`
- Icon names should not be prefixed (no `icon-` for `hoscoicon-`, etc)
- The global css prefix must be `ha`

## For developers

### Install
`yarn add gitlab:hosco/hosco-font`

### Update
`yarn upgrade hosco-font`
